# -*- coding: utf-8 -*-

"""Top-level package for e2h."""

__author__ = """IRIS PASSCAL"""
__email__ = 'software-support@passcal.nmt.edu'
__version__ = '2023.2.0.0'

#!/usr/bin/env python

"""
e2h.py convert a given epoch to a human readable format

Maeva Pourpoint
July 2020
Updates to work under Python 3.
Unit tests to ensure basic functionality.
Code cleanup to conform to the PEP8 style guide.
Directory cleanup (remove unused files introduced by Cookiecutter).
Packaged with conda.
"""

import sys
import time
import os


def get_time(epoch):
    """Take epoch in seconds and return time in gmt"""
    time_gmt = time.gmtime(epoch)
    return time_gmt


def main():
    """Takes epoch time and displays it in human readable form"""
    if len(sys.argv) != 2:
        print("USAGE: e2h epoch")
        sys.exit(1)
    os.environ['TZ'] = 'UTC'
    time.tzset()
    epoch = int(sys.argv[1])
    time_gmt = get_time(epoch)
    print("{:4d}:{:03d}:{:02d}:{:02d}:{:02d}".format(time_gmt[0], time_gmt[7],
                                                     time_gmt[3], time_gmt[4],
                                                     time_gmt[5]))


if __name__ == "__main__":
    main()

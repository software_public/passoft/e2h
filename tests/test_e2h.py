#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for `e2h` package."""

import io
import unittest

from contextlib import redirect_stdout
from unittest.mock import patch
from e2h.e2h import get_time, main

EPOCH = 1595323410
EXPECTED_DATE = '2020:203:09:23:30'


class TestE2h(unittest.TestCase):
    """Tests for `e2h` package."""

    def test_get_time(self):
        """Test basic functionality of get_time"""
        time_gmt = get_time(EPOCH)
        self.assertEqual(time_gmt[0], 2020, "Year doesn't match")
        self.assertEqual(time_gmt[1], 7, "Month doesn't match")
        self.assertEqual(time_gmt[2], 21, "Day doesn't match")
        self.assertEqual(time_gmt[3], 9, "Hour doesn't match")
        self.assertEqual(time_gmt[4], 23, "Minute doesn't match")
        self.assertEqual(time_gmt[5], 30, "Second doesn't match")
        self.assertEqual(time_gmt[7], 203, "Doy doesn't match")
        date = "{:4d}:{:03d}:{:02d}:{:02d}:{:02d}".format(time_gmt[0],
                                                          time_gmt[7],
                                                          time_gmt[3],
                                                          time_gmt[4],
                                                          time_gmt[5])
        self.assertEqual(date, EXPECTED_DATE, "Final output doesn't match")

    def test_e2h(self):
        """Test basic functionality of e2h"""
        with patch('sys.argv', ['e2h']):
            with self.assertRaises(SystemExit) as cmd:
                main()
            self.assertEqual(cmd.exception.code, 1, "sys.exit(1) never called "
                             "- e2h not fully exercised!")
        with patch('sys.argv', ['e2h ', EPOCH]):
            stdout = io.StringIO()
            with redirect_stdout(stdout):
                main()
            date = stdout.getvalue().strip()
            self.assertEqual(date, EXPECTED_DATE, "Failed to properly "
                             "convert date!")

===
e2h
===

* Description: convert a given epoch to a human readable format.

* Usage: e2h epoch

* Free software: GNU General Public License v3 (GPLv3)

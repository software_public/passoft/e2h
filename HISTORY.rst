=======
History
=======

2018.145 (2018-06-07)
------------------

* First release on new build system.

2020.203 (2020-07-21)
------------------
* Updated to work with Python 3
* Added some unit tests to ensure basic functionality of e2h
* Updated list of platform specific dependencies to be installed when
  installing e2h in dev mode (see setup.py)
* Installed and tested e2h against Python3.[6,7,8] using tox
* Formatted Python code to conform to the PEP8 style guide
* Created conda packages for e2h that can run on Python3.[6,7,8]
* Updated .gitlab-ci.yml to run a linter and unit tests for Python3.[6,7,8]
  in GitLab CI pipeline
